<%-- 
    Document   : index
    Created on : Sep 18, 2018, 2:59:23 PM
    Author     : pjera
--%>

<%@page import="lt.bit.User"%>
<%@page import="lt.bit.DB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Skaičiuoklė</h1>
        <table border="1" bordercolor="green">
            <%
                for (int i = 1; i <= 10; i++) {
            %>
            <tr>
                <%
                    for (int y = 1; y <= 10; y++) {
                %>
                <td>
                    <%= i * y%>
                </td>
                <%
                    }
                %>
            </tr>
            <%
                }
            %>
        </table> 
        <br/>
        <h1>User sąrašas</h1>
        <br>
        <a href="addUpdateUser.jsp">Pridėti</a>
        <br>
        <br>
        <table border="1">
            <tr>
                <th>Vardas</th>
                <th>Pavardė</th>
                <th>Amžius</th>
                <th>Gimimo data</th>
                <th></th>
                <th></th>
            </tr>

            <%
                for (int i = 0; i < DB.getAll().size(); i++) {
                    User user = DB.getAll().get(i);
            %>
            <tr>
                <td><%= user.getFirstName()%></td>
                <td><%= user.getLastName()%></td>
                <td><%= user.getAge()%></td>
                <td><%= user.getDate() %></td>
                <td><a href="UserDeleteServlet?id=<%= i %>">Trinti</a></td>
                <td><a href="addUpdateUser.jsp?id=<%= i %>">Atnaujinti</a></td>
            </tr>
            <%
                }
            %>
        </table>
        <br>


<br/>
        <a href="HelloWorld">Hello World</a>
        <!-- JSP expression -->
        <%= "HELLO WORLD".toLowerCase() %>
        <br>
        Is 75 less than 69? <%= 75<69 %>
    </body>
</html>
