<%-- 
    Document   : test
    Created on : Sep 20, 2018, 7:57:21 AM
    Author     : pjera
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <!-- Scriptlet testing -->
        <%
            for (int i = 0; i < 5; i++) {
                out.println("<br>Bujaka: " + i);
            }
        %>
        <br><br>
        <!-- JSP Declaration -->
        <%!
            String makeItUppercase(String data) {
                return data.toUpperCase();
            }
        %>
        <%= makeItUppercase("čia buvo mažosios raidės, rip")
        %>
    </body>
</html>
