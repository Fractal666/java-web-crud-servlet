<%-- 
    Document   : todo-demo
    Created on : Sep 20, 2018, 11:38:19 AM
    Author     : pjera
--%>
<%@page import="java.util.*"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <!-- Step 1: create HTML form -->
        <form action="todo-demo.jsp">
            Add new item: <input type="text" name="item"/>
            <input type="submit" value="Submit"/>
        </form>
        <br>Item entered: <%= request.getParameter("item")%>
        <!-- Step 2: add item to "To Do" list -->
        <%
            //get TO DO items from the session
            List<String> list = (List<String>) session.getAttribute("myToDoList");

            //if the list does not exist then create one
            if (list == null) {
                list = new ArrayList<>();
                session.setAttribute("myToDoList", list);   //name, value
            }
            //see if there is form data to add
            String theItem = request.getParameter("item");
            if (theItem != null) {
                list.add(theItem);
            }
        %>
        <!-- Step 3: Display all "To Do" items -->
        <hr>
        <b><br>To List Items</b>
        <ol>
            <%
                for(String item: list){
                    out.println("<li>"+item+"</li>");
                }
            %>
        </ol>
    </body>
</html>
