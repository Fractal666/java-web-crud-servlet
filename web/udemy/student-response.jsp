<%-- 
    Document   : student-response
    Created on : Sep 20, 2018, 9:41:50 AM
    Author     : pjera
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        Student is confirmed: <%= request.getParameter("firstName")%> <%= request.getParameter("lastName")%> jis yra iš <%= request.getParameter("country")%>
        <br>Mėgstamiausios programavimo kalbos: 
        <%
            String[] langs = request.getParameterValues("favoriteLanguage");
            if (langs != null) {
                for (String s : langs) {
                    out.println("<li>" + s + "</li>");
                }
            }
        %>

        <br><br>
        <br>Kitas būdas su alternatyvia sintakse:
        <br>Student is confirmed: ${param.firstName} ${param.lastName} jis yra iš ${param.country}
        <br>Mėgstamiausia programavimo kalba: ${param.favoriteLanguage}
    </body>
</html>
