<%-- 
    Document   : homepage
    Created on : Sep 20, 2018, 9:05:40 AM
    Author     : pjera
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="my-header.html"/>
        <h3>Regular content</h3>
        Some info, lorem ipsum <br><br>
        Some info, lorem ipsum <br><br>
        Some info, lorem ipsum <br><br>
        Some info, lorem ipsum <br><br>
        
        <jsp:include page="my-footer.jsp"/>
    </body>
</html>
