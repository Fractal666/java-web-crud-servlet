<%-- 
    Document   : updateUser
    Created on : Sep 20, 2018, 2:38:58 PM
    Author     : pjera
--%>

<%@page import="lt.bit.DB"%>
<%@page import="lt.bit.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        <%!
            String id;
            User user;
            String firstName = "";
            String lastName ="";
            int age = 0;
            String action = "UserAddServlet";
            String date = "";
        %>
        <% id = request.getParameter("id");%>
        <%
            if (id != null) {
                user = DB.getAll().get(Integer.parseInt(id));
                firstName = user.getFirstName();
                lastName = user.getLastName();
                age = user.getAge();
                action = "UserUpdateServlet"+"?id="+id;
                date = user.getDate().toString();
            } else{
                
            }
        %>

        <br>

        <form action="<%= action%>" method="POST">
            First name:<br>
            <input type="text" name="firstName" value="<%= firstName%>"><br>
            Last name:<br>
            <input type="text" name="lastName" value="<%= lastName%>"><br>
            Amžius:<br>
            <input type="number" name="age" value="<%= age%>"><br>
            Gimimo data:<br>
            <input type="text" name="date" value="<%= date%>"<br><br>
            <% id = null; %>
            <input type="submit" value="Saugoti" />
            <a href="index.jsp?id=">Cancel</a>
        </form>  
    </body>
</html>
