<%-- 
    Document   : builtin-test
    Created on : Sep 20, 2018, 8:36:26 AM
    Author     : pjera
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h3>JSP Built-in Objects</h3>
        Request user agent: <%= request.getHeader("User-Agent") %>
        
        <br>
        <br>
        Request language: <%= request.getLocale() %>
        <br><br>
        <%= request.getServletPath() %>
                
    </body>
</html>
