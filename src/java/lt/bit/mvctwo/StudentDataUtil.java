/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bit.mvctwo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pjera
 */
public class StudentDataUtil {
    
    public static List<Student> getStudents(){
        
        //create an empty list
        List<Student> list = new ArrayList<>();
        
        //add sample data
        list.add(new Student("Petras", "Jasinskas", "petras@jasinskas.lt"));
        list.add(new Student("Kitas", "Studentas", "some@some.lt"));
        list.add(new Student("Antras", "Studentauskas", "main@email.lt"));
        
        //return the list
        return list;
    }
    
}
