/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bit;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pjera
 */
public class DB {

    private static final List<User> list = new ArrayList<>();
    static {
        User u = new User("Petras", "Petraitis", 30, "1990-09-20");
        list.add(u);
        u = new User("Jonas", "Jonaitis", 20, "1980-01-20");
        list.add(u);
        u = new User("Marija", "Melnikaitė", 54, "1950-07-20");
        list.add(u);
    }
    public static List<User> getAll() {
        return list;
    }

    public static void addUser(User user) {
        if (user != null) {
            list.add(user);
        }
    }
    
    public static User removeUser(int id){
        for(int i=0; i<list.size(); i++){
            if(id == list.get(i).getId()){
                return list.remove(i);
            }
        }
        return null;
    }
    
    public static User updateUser(User user){
        int id = user.getId();
        int foundId=-1;
        for(int i=0; i<list.size(); i++){
            if(id == list.get(i).getId()){
                foundId = i;
                break;
            }
        }
        list.set(foundId, user);
        return user;
    }
}
