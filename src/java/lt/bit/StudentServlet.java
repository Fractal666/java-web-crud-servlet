/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.jasinskas;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author pjera
 */
@WebServlet(name = "StudentServlet", urlPatterns = {"/StudentServlet"})
public class StudentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //Step 1: set the content type
       response.setContentType("text/html;charset=UTF-8");
       
       //Step 2: get the printWriter
       PrintWriter out = response.getWriter();
       
       //step 3: generate the HTML content
       out.println("<html><body>");
       out.println("The student is confirmed: "
               + request.getParameter("firstName")+ " "
               + request.getParameter("lastName"));
       out.println("</body></html>");

    }
}
