/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bit;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author pjera
 */
public class User {

    private static int counter = 0;
    private int id;
    private String firstName;
    private String lastName;
    private int age;
    private Date date;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public User(String firstName, String lastName, int age, String date) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.id = counter++;
        try {
            this.date = new SimpleDateFormat("yyyy-MM-dd").parse(date);
        }catch(ParseException e){
            
    }
}

public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String format = formatter.format(date);
        return format;
    }

    public void setDate(String date) throws ParseException{
        this.date = new SimpleDateFormat("yyyy-MM-dd").parse(date);
    }
    
    

    @Override
        public String toString() {
        return "<br>"+"First Name: "+firstName+"Last Name: "+lastName+" ";
    }    
    
}
